# Laravel Básico

## Sobre

O Laravel Básico é um kit inicial para aplicação web desenvolvida em Laravel, utilizando Livewire.

## Instalação

O projeto está configurado para ser executado com docker-compose.

Primeiro configure o arquivo .env baseado no arquivo .env.example. Após isso, execute o comando: docker-compose up para iniciar os containers.

## Comandos úteis

### Comando para Migration

- Executar migrations: *docker exec planning_poker_app php artisan migrate*
- Verificando o status das migrations: *docker exec planning_poker_app php artisan migrate:status*
- Executando seeds: *docker exec planning_poker_app php artisan db:seed --class=ExemploSeeder*

### Comando do Laravel

- Gerando chave do projeto: *docker exec planning_poker_app php artisan key:generate*
- Reconfigurar cache e rotas: *docker exec planning_poker_app php artisan optimize*
- Criando model e migration: *docker exec planning_poker_app php artisan make:model Exemplo -m*

### Comando do Livewire

- Criando componente: *docker exec planning_poker_app php artisan livewire:make ShowExemplo*