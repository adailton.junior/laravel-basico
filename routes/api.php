<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthenticateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('phpinfo', function () {
    echo phpinfo();
});

Route::group(['prefix' => 'v1'], function () {
    //public area
    Route::get('', function () {
        return response()->json(['status' => 'API_ONLINE']);
    });

    Route::post('authenticate', [AuthenticateController::class, 'authenticate']);


    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('authenticate', [AuthenticateController::class, 'isAuthenticate']);

        Route::get('/user', function (Request $request) {
            return $request->user();
        });

        Route::resource('users', UsersController::class);
    });
});