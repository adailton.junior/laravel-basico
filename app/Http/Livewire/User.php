<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User as Users;

class User extends Component
{
    public $users;
    public $name, $email, $password;
    public $isModalOpen = false;

    public function render()
    {
        $this->users = Users::select('id', 'name', 'email')->get();
        return view('livewire.users.list');
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetCreateForm(){
        $this->name = '';
        $this->email = '';
        $this->password = '';
    }
    
    public function store()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
    
        Users::updateOrCreate(['id' => $this->id], [
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
        ]);
        session()->flash('message', $this->id ? 'Usuário atualizado.' : 'Usuário criado.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public function edit($id)
    {
        $user = Users::findOrFail($id);
        $this->id = $id;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->mobile = $user->mobile;
    
        $this->openModalPopover();
    }
    
    public function delete($id)
    {
        Users::find($id)->delete();
        session()->flash('message', 'Usuário deletado.');
    }
}
