<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

use Log;

trait Actions
{

    public function index(Request $request)
    {
        $classe = $this->getModel();
        $lista = $classe::get();

        return $lista;
    }

    public function show(Request $request, $id)
    {
        $classe = $this->getModel();
        $model = $classe::findOrFail($id);

        return $model;
    }
    
    public function store(Request $request)
    {
        $classe = $this->getModel();
        $model = new $classe();

        return $this->saveOrUpdate($request, $model, 'Store');
    }

    public function update(Request $request, $id)
    {
        $classe = $this->getModel();
        $model = $classe::find($id);

        return $this->saveOrUpdate($request, $model, 'Update');
    }

    protected function saveOrUpdate(Request $request, $model, $action)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, $this->getValidationRules($request, $model));
    
        // Check validation failure
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $model->fill($input);

        \DB::transaction(function () use ($request, $action, $model) {
            $model->save();
        });

        return $model;
    }

    public function destroy(Request $request, $id)
    {
        $classe = $this->getModel();
        $model = $classe::find($id);

        \DB::transaction(function () use ($request, $model) {
            $model->delete();
        });
    }
}
