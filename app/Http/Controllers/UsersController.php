<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;

use App\Models\User;

class UsersController extends CrudController
{
    protected function getModel() {
        return User::class;
    }

    function getValidationRules(Request $request, $model) {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users'
        ];

        if (strpos($request->route()->getName(), 'users.update') !== false) {
            $rules['email'] = 'required|email|max:255|unique:users,email,'.$model->id;
        }

        if (strpos($request->route()->getName(), 'users.store') !== false) {
            $rules['password'] = ['required', Password::min(8)->mixedCase()->letters()->numbers()->symbols()];
        }


        return $rules;
    }
}
