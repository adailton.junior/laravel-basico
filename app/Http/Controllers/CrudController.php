<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\Actions;

abstract class CrudController extends Controller
{
    use Actions;

    /**
     * Get the model class.
     *
     * @return string
     */
    abstract protected function getModel();
}
