<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Log;

class AuthenticateController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
 
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken($user->email);
            return response()->json(['user' => $user, 'token' => $token->plainTextToken]);
        }
 
        return response()->json([
            'error' => 'Credenciais invalidas.',
        ]);
    }

    public function isAuthenticate(Request $request) {
        $user = Auth::user();
        return response()->json([
            'message' => 'token válido!'
        ]);
    }
}
