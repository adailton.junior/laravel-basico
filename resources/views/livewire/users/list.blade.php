<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Usuários') }}
    </h2>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <p class="text-sm">{{ session('message') }}</p>
                    </div>
                </div>
            </div>
            @endif
            <div class="flex justify-end">
                <button wire:click="create()"
                    class="my-4 inline-flex justify-center w-24 rounded-md border border-transparent px-4 py-2 bg-red-600 text-base font-bold text-white shadow-sm hover:bg-red-700">
                    <i class="fa-solid fa-plus"></i>
                </button>
            </div>
            @if($isModalOpen)
                @include('livewire.users.create')
            @endif
            <table class="table-fixed w-full">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="px-4 py-2">Nome</th>
                        <th class="px-4 py-2">E-mail</th>
                        <th class="px-4 py-2">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td class="border px-4 py-2">{{ $user->name }}</td>
                        <td class="border px-4 py-2">{{ $user->email}}</td>
                        <td class="border px-4 py-2">
                            <div class="flex justify-center">
                                <button wire:click="edit({{ $user->id }})"
                                    data-toggle="tooltip" data-placement="top" title="Editar"
                                    class="w-24 px-4 py-2 bg-gray-500 text-gray-900 cursor-pointer"><i class="fa-solid fa-pen"></i></button>
                                <button wire:click="delete({{ $user->id }})"
                                    data-toggle="tooltip" data-placement="top" title="Remover"
                                    class="w-24 px-4 py-2 bg-red-100 text-gray-900 cursor-pointer"><i class="fa-solid fa-trash"></i></button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>