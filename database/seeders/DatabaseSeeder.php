<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $existAdmin = User::where('name', 'Administrador')->exists();
        if (!$existAdmin){
            User::factory()->createAdmin()->create();
        }
        User::factory(10)->create();
    }
}
